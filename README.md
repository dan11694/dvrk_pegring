# dvrk_pegring

packages and tools for autonomous task with dvrk based on ASP and DMPs.

CITATION:
When using this framework in your research, please cite as follows:

@article{ginesi2020autonomous,
  title={Autonomous task planning and situation awareness in robotic surgery},
  author={Ginesi, Michele and Meli, Daniele and Roberti, Andrea and Sansonetto, Nicola and Fiorini, Paolo},
  journal={arXiv preprint arXiv:2004.08911},
  year={2020}
}

DEPENDENCIES:
- CoppeliaSim https://www.coppeliarobotics.com/downloads
- install clingo following instructions at https://github.com/potassco/clingo (clingo 5.3 tested in this repo)
- build ROS packages from https://github.com/jhu-dvrk/dvrk-ros (CISST 1.7)
- build peg_ring package from https://gitlab.com/Seirin/peg_and_ring (branch simulation for simulated task on V-REP)


DESCRIPTION:
- dvrk_task_msgs contains custom msgs (action msgs expect fields defining the action id, the agent robot, the operated object and its property, as of standard granularity definitions for actions)
- robot_control contains tools for dvrk control and motion control with DMPs
- adaptive_controller contains tools for ASP management with Clingo

TESTING IN SIMULATION (pegring task): 
- open scene pegring_scene.ttt in CoppeliaSim
- roslaunch robot_control dvrk_console.launch
- roslaunch peg_and_ring peg_ring.launch
- roslaunch robot_control framework.launch

NOTES:
- for real pegring, omit sections of codes marked as "V-REP..." in robot_control/scripts/motion_pegring.py, robot_control/scripts/sensing_pegring.py (maybe some tolerances in these two files need to be adjusted, e.g. for target poses and failure conditions)
- for other tasks, modify sensing_pegring.py and motion_pegring.py. In sensing, define appropriate functions for compute_fluents, compute_target and check_failure. In motion, define appropriate motion_policies in execution. Also modify the description file in robot_control/config/pegring.json, with definition of appropriate actions and properties (motion policy name, type either dmp or custom, and weigths for DMPs). Then, in framework.launch, change the params task_name, asp_name (for asp file in adaptive_controller/asp), and the name of sensing node, and define the correct motion control file in robot_control/scripts/control.py in the code section "DEFINE MOTION CONTROL"
- When running nodes on a separate computer, dvrk_task_msgs must be built also there!

