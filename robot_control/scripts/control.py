#!/usr/bin/env python
import numpy as np
import math
import os
import roslib
import rospkg
import rospy
import actionlib
import json
import sys
import copy
from std_msgs.msg import Int32, Bool
from dvrk_task_msgs.msg import ActionRequestFiner, ActionArray
import dvrk




def main():

    rospy.init_node('TaskDVRK')

    rospack = rospkg.RosPack()
    path = rospack.get_path("robot_control") + '/config/'
    task = rospy.get_param("task_name")
    with open(path + task + ".json") as f:
        actions = json.load(f)["actions"]

    #DEFINE MOTION CONTROL
    if task == "pegring":
        from motion_pegring import motion_manager
    else:
        from motion_pegring import motion_manager
    #DEFINE MOTION CONTROL
    
    motion = motion_manager()
    rospy.sleep(1.)

    #ASK FOR FLUENTS
    motion.sensing_pub.publish(Int32(1))

    while not rospy.is_shutdown():

        rospy.loginfo('Waiting for the next action from the reasoner...')
        init_time = rospy.Time.now()
        while motion.state == []:
            if rospy.Time.now() - init_time > rospy.Duration(5.):
                #ASK FOR FLUENTS
                motion.sensing_pub.publish(Int32(1))
                init_time = rospy.Time.now()
        
        for i in range(len(motion.state)):
            current_action = [a for a in actions if a["name"]==motion.state[i] and a["object"]==motion.location[i]][0]
            motion.agents.append(motion.manip_id[i])
            motion.policies.append(current_action["policy"])
            motion.policy_types.append(current_action["policy_type"])
            if current_action["policy_type"] == "dmp":
                motion.dmp_weights.append(current_action["weights"])
            motion.n_arms.append(current_action["n_arms"])

        motion.execute()

        #FEEDBACK
        if not motion.failure:
            end_msg = Bool(True)
            
        else: #FAILURE -> RAISE PSMs TO FREE CAMERA VISION AND COMPUTE NEW FLUENTS
            # motion.recovery()
            end_msg = Bool(False)

            #CHECKING FLUENTS
            rospy.loginfo('Waiting for the fluents from the camera...') 
            motion.computed_fluents = False
            motion.sensing_pub.publish(Int32(1))
            while not motion.computed_fluents:
                pass

        motion.reset()
        motion.end_pub.publish(end_msg)

    rospy.spin()


if __name__ == '__main__':
    main()