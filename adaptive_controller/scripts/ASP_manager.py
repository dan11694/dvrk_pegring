#!/usr/bin/env python

import rospy
import rospkg
from clingo import Function as Fun, Control, SolveResult, Model
from dvrk_task_msgs.msg import ContextModel, ActionRequestFiner, ActionArray
from std_msgs.msg import Bool
import copy
from gringoParser import string2fun as parse



class Listener(object):
    def __init__(self):
        self.feedback = None
        self.context = None
        self.received_context = False
        rospy.Subscriber('/context/model', ContextModel, self.context_cb)
        rospy.Subscriber('/action_feedback', Bool, self.feedback_cb)


    def feedback_cb(self, data):
        self.feedback = data.data

    def context_cb(self, data):
        self.context = copy.deepcopy(data)
        self.received_context = True










class Solver(object):    
    def __init__(self, filename):
        self.filename = filename

        self.control = Control([])
        path = rospkg.RosPack().get_path('adaptive_controller')
        self.control.load(path + '/asp/' + self.filename)
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.ordered_atoms = []
        self.action_time = 0  
        self.action_index = 0
        self.step = 1 #for iterative solver

    def restart(self):
        self.control = Control([])
        path = rospkg.RosPack().get_path('adaptive_controller')
        self.control.load(path + '/asp/' + self.filename)
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.step = 1 #for iterative solver
        

    def on_model(self, model):
        self.atoms[:] = model.symbols(shown=True)
        self.hidden_atoms[:] = model.symbols(atoms=True)

    def solve(self, context):
        self.ordered_atoms = copy.deepcopy(self.ordered_atoms[0:self.action_index])
        #DELETE EXTERNALS FROM PREVIOUS CALL
        for atom in self.hidden_atoms:
            self.control.assign_external(atom, False)
        #GROUND NEW EXTERNALS
        for atom in context.atoms:
            self.control.assign_external(parse(atom), True)

        init_time = rospy.Time.now()
        while rospy.Time.now() - init_time < rospy.Duration(secs=100.):
            parts = []

            self.control.cleanup()
            parts.append(("check", [self.step]))
            self.control.release_external(Fun("query", [self.step-1]))

            parts.append(("step", [self.step]))

            self.control.ground(parts)
            self.control.assign_external(Fun("query", [self.step]), True)
            result = self.control.solve(on_model = self.on_model)
            self.step += 1
            if result.satisfiable:
                break

        if result.satisfiable:
            if self.atoms != []:
                rospy.loginfo("FOUND AN ANSWER SET!")
                for atom in self.atoms:
                    tmp_list = ['none', 'none', 'none', 'none', 0]
                    tmp_list[0] = atom.name # name...
                    for i in range(len(atom.arguments)-1): # ...arguments...
                        tmp_list[i+1] = str(atom.arguments[i])
                    tmp_list[-1] = int(str(atom.arguments[-1])) + self.action_time # ...time
                    self.ordered_atoms.append(tmp_list)

                self.ordered_atoms.sort(key = lambda action: action[-1])
                rospy.loginfo('SEQUENCE OF POSSIBLE ACTIONS IS: ')
                rospy.loginfo(self.ordered_atoms)

        else:
            rospy.signal_shutdown('UNSATISFIABLE PLANNING! EXITING...')

    def get_action_msg(self):
        actions = ActionArray()
        while len(self.ordered_atoms) > self.action_index and self.ordered_atoms[self.action_index][-1] == self.action_time + 1:
            action = ActionRequestFiner()
            action.robot = str(self.ordered_atoms[self.action_index][1])
            action.action = str(self.ordered_atoms[self.action_index][0])
            action.object = str(self.ordered_atoms[self.action_index][2])
            action.color = str(self.ordered_atoms[self.action_index][3])

            actions.action_list.append(action)
            self.action_index += 1

        if actions != ActionArray():
            self.action_time += 1
        return actions












def main():
    rospy.init_node('ASP_manager')
    filename = rospy.get_param("asp_name")

    listener = Listener()
    solver = Solver(filename)
    action_pub = rospy.Publisher('/actions/request', ActionArray, queue_size=1)

    while not rospy.is_shutdown():
        #GETTING FLUENTS
        if listener.received_context:
            rospy.loginfo('FLUENTS ARE:')
            rospy.loginfo(listener.context)
            solver.restart()
            solver.solve(listener.context)
            listener.received_context = False

        action = solver.get_action_msg()
        action_pub.publish(action)

        if action != ActionArray():
            #ACTION FEEDBACK
            rospy.loginfo("WAITING FOR ACTION FEEDBACK...")
            while listener.feedback is None:
                pass
            listener.feedback = None      

    rospy.spin()








if __name__ == '__main__':
    main()
